#!/usr/bin/python
import logging
import SENSOR_DHT11Temp
import SENSOR_MQ2_GAS
import HCSR04UltraSonic
import RPi.GPIO as GPIO
import time

def setupPIR(pir):
    GPIO.setmode(GPIO.BOARD)                          #Set GPIO pin numbering
    GPIO.setup(pir, GPIO.IN)
    time.sleep(2)
    return

def getUltrasonicDist(pin_one, pin_two):
  GPIO.setmode(GPIO.BCM)                     #Set GPIO pin numbering 

  TRIG = pin_one                                 #Associate pin 23 to TRIG
  ECHO = pin_two                                  #Associate pin 24 to ECHO



  print("Distance measurement in progress")

  GPIO.setup(TRIG,GPIO.OUT)                  #Set pin as GPIO out
  GPIO.setup(ECHO,GPIO.IN)                   #Set pin as GPIO in

  while True:

    GPIO.output(TRIG, False)                 #Set TRIG as LOW
    print("Waitng For Sensor To Settle")
    time.sleep(2)                            #Delay of 2 seconds

    GPIO.output(TRIG, True)                  #Set TRIG as HIGH
    time.sleep(0.00001)                      #Delay of 0.00001 seconds
    GPIO.output(TRIG, False)                 #Set TRIG as LOW

    while GPIO.input(ECHO)==0:               #Check whether the ECHO is LOW
      pulse_start = time.time()              #Saves the last known time of LOW pulse

    while GPIO.input(ECHO)==1:               #Check whether the ECHO is HIGH
      pulse_end = time.time()                #Saves the last known time of HIGH pulse 

    pulse_duration = pulse_end - pulse_start #Get pulse duration to a variable

    distance = pulse_duration * 17150        #Multiply pulse duration by 17150 to get distance
    distance = round(distance, 2)            #Round to two decimal points

    if distance > 2 and distance < 400:      #Check whether the distance is within range
      return (distance - 0.5)  #Print distance with 0.5 cm calibration
    else:
      return -1                   #display out of range
    return -1

                          
def updatePIR(pir):
   if GPIO.input(int(pir)):                            #Check whether pir is HIGH
      time.sleep(2)
      return "Y"
   else:
       return "N"

def sendDataToPC(info):
        try:
            file = open("/home/pi/.config/AGKApps/raspberryPIToComputer/media/Message.txt","w")
            file.write(info)
            file.close()
            return
        except:
            return

def decode(code):
    try:
        if "_" in code:
                print("has _")
                info = code.split("_")
                if(len(info) > 0):
                        print("code split length is more then 0")
                        pin = info[0]
                        addr = info[1]

                        print(pin)
                        print(addr)


                        if "PIR" in addr:
                                print("starting pir")
                                setupPIR(int(pin))
                                _result = updatePIR(pin)
                                print("PIR data: {0}".format(_result))
                                sendDataToPC(_result)
                        if "ULTRASONIC" in addr:
                            print("starting ultrrasonic")
                            _result = getUltrasonicDist(int(pin),int(pin) +1)
                            print(_result)
                            sendDataToPC(str(_result))
                            
                        if(addr == "TOGGLE"):
                            #sendDataToPC(_result)
                            _OUT = info[2]
                            GPIO.setup(pin, GPIO.OUT)
                            GPIO.output(pin, _OUT)
                    
    except ValueError as err:
        print("error trying to read main file, you might want to ignore this")
        print("details: {0}".format(err))
        logging.exception(err) 
    return
print("running...")
while True:
    try:  
        file = open("/home/pi/.config/AGKApps/raspberryPIToComputer/media/MessageFromClient.txt","r")
        stuff = file.read();
        file.close()
        decode(stuff)
        
        
        
    except ValueError as err:
        print("error trying to read main file, you might want to ignore this")
        print("details: {0}".format(err))
        logging.exception(err)






def readhardware():
            files = open("/home/pi/.config/AGKapps/raspberryPIToComputer/media/hardware_code.txt","r")
            _codes = files.readlines()
            files.close()
            return



