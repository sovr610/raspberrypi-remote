#!/usr/bin/python
import RPi.GPIO as GPIO                           #Import GPIO library
import time

def setupPIR(pir):
    GPIO.setmode(GPIO.BOARD)                          #Set GPIO pin numbering
    GPIO.setup(pir, GPIO.IN)
    time.sleep(2)
    return


                          
def updatePIR(pir):
   if GPIO.input(pir):                            #Check whether pir is HIGH
      time.sleep(2)
      return "Y"
   else:
       return "N"
                                   #While loop delay should be less than detection(hardware) delaydef functionone(str):
   
