
// Project: RaspberryPiRemote 
// Created: 2016-09-10

// show all errors
//SetErrorMode(2)

// set window properties
SetWindowTitle( "RaspberryPiRemote" )
SetWindowSize( 1024, 768, 0 )

// set display properties
SetVirtualResolution( 1024, 768 )
SetOrientationAllowed( 1, 1, 1, 1 )
SetSyncRate( 30, 0 ) // 30fps instead of 60 to save battery

global networkName as String
global hostName as string
global portNumber as integer

SetErrorMode(2)
SetPrintSize(16)
c = 0
while c = 0
	print("would you lik to edit the connection settings?(Y/N): ")
	ff = GetRawLastKey()
	if ff <> -1
		if ff = asc("Y") 
			c = 1
		endif
		
		if ff = asc("N")
			c = 1
		endif
	endif
	sync()
endwhile 
if ff = asc("Y")
	StartTextInput("enter network name!")
    do
        sync()
        if GetTextInputCompleted()
            if GetTextInputCancelled()
                networkName="default"
            else
                networkName=GetTextInput()
                exit
            endif
        endif
    loop
    StopTextInput()
    
    StartTextInput("now enter this programs client name")
    do
        sync()
        
        if GetTextInputCompleted()
            if GetTextInputCancelled()
                hostName="defaultHost"
            else
                hostName=GetTextInput()
                exit
            endif
        endif
    loop
    StopTextInput()
	
	StartTextInput("enter port number")
    do
        sync()
        if GetTextInputCompleted()
            if GetTextInputCancelled()
                portNumber=4444
            else
                portNumber=val(GetTextInput())
                exit
            endif
        endif
    loop
    StopTextInput()
	
	if getfileexists("system.txt") = 1
		deletefile("system.txt")
	endif
	
	f = opentowrite("system.txt")
	writestring(f, networkName)
	writestring(f, hostName)
	writestring(f, str(portNumber))
	closefile(f)
endif


if GetFileExists("system.txt") = 1
	f = opentoread("system.txt")
	networkName = readstring(f)
	hostName = readstring(f)
	portNumber = val(readstring(f))
	CloseFile(f)
else
	f = opentowrite("system.txt")
	writestring(f,"default")
	writestring(f,"hostname")
	writestring(f,"4444")
	closefile(f)
endif

global net as integer
	net = HostNetwork(networkName, hostName, portNumber)
	print("Network is setup as host:")
	printc("networkName: ")
	print(networkName)
	printc("hostName: ")
	print(hostName)
	printc("portNumber: ")
	print(str(portNumber))




do
	msg$ = getMessage()
	if msg$ <> ""
		g = opentowrite("MessageFromClient.txt")
		writestring(g,msg$)
		closefile(g)
	endif
	
	sleep(200)
	
	printc("networkName: ")
	print(networkName)
	printc("hostName: ")
	print(hostName)
	printc("portNumber: ")
	print(str(portNumber))
    Print( ScreenFPS() )
    Sync()
    
    SendMessage()
loop

function getMessage()
	msg = GetNetworkMessage(net)
	if msg > 0
		str$ = GetNetworkMessageString(msg)
		printc("got message: ")
		print(str$)
		DeleteNetworkMessage(msg)
	endif
endfunction str$

function SendMessage()
	if getfileexists("message.txt") = 1
		g = opentoread("message.txt")
		mes$ = readstring(g)
		closefile(g)
		if mes$ <> ""
			msg = CreateNetworkMessage()
			AddNetworkMessageString(msg,mes$)
			SendNetworkMessage(net, 0, msg)
			sleep(100)
			DeleteNetworkMessage(msg)
		endif
	endif
	deletefile("message.txt")
endfunction
			
