﻿

Namespace RaspberryPiRemoteCSharp
    Friend Class PADtoRaspberryPi
        Private _agk As AGKInfoGrabber

        Public Sub New()
            _agk = New AGKInfoGrabber()

            runRaspberryPi()
        End Sub

        Public Sub sendPADMachineCodeToRaspberryPI(code As String)
            _agk.CreateAGKTwoFileContent("message.txt", code, "RaspberryPiRemote")
        End Sub

        Public Function getPADMachineCodeFromRaspberryPI() As String
            Try
                Return _agk.getAGKTwoFileContent("MessageFromClient.txt", "RaspberryPiRemote")(0)
            Catch i As Exception
                Return ""
            End Try
        End Function

        Public Sub runRaspberryPi()
            Try
                System.Diagnostics.Process.Start(Environment.CurrentDirectory + "\raspberryPiRemote.exe")
            Catch ex As Exception
                'Log.handleException(ex);
                Console.WriteLine(ex)
            End Try
        End Sub


        'code setup:
        'PIN_TYPE
        'VALUE -> sending a value to the motor or function
        'PIN -> the pin of where the 
        Public Function checkHumiditySensor(pin As String, ByRef _loop As Boolean) As String
            'the loop is a way for the outside code calling this function to end the loop
            Try
                _loop = True
                Dim code As String = pin & Convert.ToString("_HUMIDITYTEMP")
                Me.sendPADMachineCodeToRaspberryPI(code)
                Dim result As String = ""
                While result = "" OrElse result Is Nothing AndAlso _loop = True
                    result = getPADMachineCodeFromRaspberryPI()
                End While

                Return result
            Catch i As Exception
                'Log.handleException(i);
                Console.WriteLine(i.Message)
                Return Nothing
            End Try
        End Function

        Public Function checkTemperatureSensor(pin As String, ByRef _loop As Boolean) As String
            'the loop is a way for the outside code calling this function to end the loop
            Try
                _loop = True
                Dim code As String = pin & Convert.ToString("_HUMIDITYTEMP")
                Me.sendPADMachineCodeToRaspberryPI(code)
                Dim result As String = ""
                While result = "" OrElse result Is Nothing AndAlso _loop = True
                    result = getPADMachineCodeFromRaspberryPI()
                End While

                Return result
            Catch i As Exception
                Console.WriteLine(i.Message)
                Return Nothing
            End Try
        End Function

        Public Function checkPIRsensor(pin As String, ByRef _loop As Boolean) As String
            'the loop is a way for the outside code calling this function to end the loop
            Try
                _loop = True
                Dim code As String = pin & Convert.ToString("_PIR")
                Me.sendPADMachineCodeToRaspberryPI(code)
                Dim result As String = ""
                While result = "" OrElse result Is Nothing AndAlso _loop = True
                    result = getPADMachineCodeFromRaspberryPI()
                End While

                Return result
            Catch i As Exception
                Console.WriteLine(i.Message)
                Return Nothing
            End Try
        End Function

        Public Function checkGasSensorPercentage(pin As String, ByRef _loop As Boolean) As String
            'the loop is a way for the outside code calling this function to end the loop
            Try
                _loop = True
                Dim code As String = pin & Convert.ToString("_GAS")
                Me.sendPADMachineCodeToRaspberryPI(code)
                Dim result As String = ""
                While result = "" OrElse result Is Nothing AndAlso _loop = True
                    result = getPADMachineCodeFromRaspberryPI()
                End While

                Return result
            Catch i As Exception
                Console.WriteLine(i.Message)
                Return Nothing
            End Try
        End Function

        Public Function checkUltrasonicSensor(pin As String, ByRef _loop As Boolean) As String
            'the loop is a way for the outside code calling this function to end the loop
            Try
                _loop = True
                Dim code As String = pin & Convert.ToString("_ULTRASONIC")
                Me.sendPADMachineCodeToRaspberryPI(code)
                Dim result As String = ""
                While result = "" OrElse result Is Nothing AndAlso _loop = True
                    result = getPADMachineCodeFromRaspberryPI()
                End While

                Return result
            Catch i As Exception
                Console.WriteLine(i.Message)
                Return Nothing
            End Try
        End Function

        Public Sub setToggleToPin(pin As String, _output As String)
            Dim code As String = Convert.ToString(pin & Convert.ToString("_TOGGLE_")) & _output
            Me.sendPADMachineCodeToRaspberryPI(code)
        End Sub




    End Class
End Namespace

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
