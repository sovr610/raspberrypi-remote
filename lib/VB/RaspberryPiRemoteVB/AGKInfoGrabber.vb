﻿
#Region "using region"

Imports System.IO
Imports System.Security.Principal

#End Region

Namespace RaspberryPiRemoteCSharp
    Friend Class AGKInfoGrabber
        Private ReadOnly dir As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.None)

        Public Sub New()
            Console.WriteLine(Convert.ToString("agk dir: ") & dir)
        End Sub

        ''' <summary>
        '''     To Grab an app game kit 1.0v file info from the AGK directory (AGK is used for seperate programs with PAD)
        ''' </summary>
        ''' <param name="filename">filename</param>
        ''' <param name="application">the AGK application name</param>
        ''' <returns>filename info</returns>
        Public Function getAGKFileContent(filename As String, application As String) As String()
            If File.Exists(Convert.ToString((Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\") & filename) Then
                Dim stuff = File.ReadAllLines(Convert.ToString((Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\") & filename)
                File.Delete(Convert.ToString((Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\") & filename)
                Return stuff
            End If
            Return Nothing
        End Function

        ''' <summary>
        '''     To Grab an app game kit 2.0v file info from the AGK directory (AGK is used for seperate programs with PAD)
        ''' </summary>
        ''' <param name="filename">filename</param>
        ''' <param name="application">the AGK application name</param>
        ''' <returns>the filename content</returns>
        Public Function getAGKTwoFileContent(filename As String, application As String) As String()
            If File.Exists(getAGKDirTwo(application, filename)) Then
                Dim stuff = File.ReadAllLines(getAGKDirTwo(application, filename))
                File.Delete(getAGKDirTwo(application, filename))
                Return stuff
            End If
            Return Nothing
        End Function

        ''' <summary>
        '''     To send info to the main AGK program that runs along side of PAD
        ''' </summary>
        ''' <param name="cmd">command</param>
        ''' <returns>To see if it worked or not</returns>
        Public Function sendCodeHelperCommand(cmd As String) As Boolean
            Try
                Dim args As String() = {cmd}
                'System.IO.File.WriteAllLines("console_cmd.txt", args);
                Console.WriteLine(getAGKDirTwo("RobotClientAppStore", "console_cmd.txt"))
                File.WriteAllLines(getAGKDirTwo("RobotClientAppStore", "console_cmd.txt"), args)
                Return True
            Catch i As Exception
                Console.WriteLine(i.Message)
                Return False
            End Try
        End Function

        ''' <summary>
        '''     create AGK 1.0 file with content, to communicate to the AGK program
        ''' </summary>
        ''' <param name="filename">the filename</param>
        ''' <param name="info">the file content (message)</param>
        ''' <param name="application">the AGK program name</param>
        Public Sub CreateAGKFileContent(filename As String, info As String, application As String)
            File.WriteAllText(Convert.ToString((Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\") & filename, info)
        End Sub

        ''' <summary>
        '''     create AGK 2.0 file with content, to communicate to the AGK program
        ''' </summary>
        ''' <param name="filename">the filename</param>
        ''' <param name="info">the file content (message)</param>
        ''' <param name="application">the AGK program name</param>
        Public Sub CreateAGKTwoFileContent(filename As String, info As String, application As String)
            File.WriteAllText(getAGKDirTwo(application, filename), info)
        End Sub

        ''' <summary>
        '''     Get the AGK 1.0 directory as a string of a program
        ''' </summary>
        ''' <param name="application">The Application name</param>
        ''' <returns>return AGK directory of AGK program</returns>
        Public Function getAGKDir(application As String) As String
            Return (Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\"
        End Function

        ''' <summary>
        '''     If there is more then one line in the AGK 1.0 file. get one of the lines
        ''' </summary>
        ''' <param name="filename">filename</param>
        ''' <param name="application">application name</param>
        ''' <param name="index">the line number</param>
        ''' <returns>the message</returns>
        Public Function getSubItemOfAGKFile(filename As String, application As String, index As Integer) As String
            If File.Exists(Convert.ToString((Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\") & filename) Then
                Return File.ReadAllLines(Convert.ToString((Convert.ToString(dir & Convert.ToString("\AGK\")) & application) + "\media\") & filename)(index)
            End If
            Return Nothing
        End Function

        ''' <summary>
        '''     get the AGK 2.0 directory based on a program
        ''' </summary>
        ''' <param name="appName">the program name</param>
        ''' <param name="filename">the filename name</param>
        ''' <returns>the directory of the file</returns>
        Public Function getAGKDirTwo(appName As String, filename As String) As String
            Dim security = WindowsIdentity.GetCurrent().Name

            Dim security_name = security.Split("\"c)
            Return Convert.ToString((Convert.ToString("C:\Users\" + security_name(1) + "\AppData\Local\AGKApps\") & appName) + "\media\") & filename
        End Function


    End Class
End Namespace

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================

