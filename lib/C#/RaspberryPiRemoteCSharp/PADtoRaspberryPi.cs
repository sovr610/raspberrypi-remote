using System;

namespace RaspberryPiRemoteCSharp
{
    internal class PADtoRaspberryPi
    {
        private AGKInfoGrabber _agk;

        public PADtoRaspberryPi()
        {
            _agk = new AGKInfoGrabber();
            runRaspberryPi();
           
        }

        public void sendPADMachineCodeToRaspberryPI(string code)
        {
            _agk.CreateAGKTwoFileContent("message.txt", code, "RaspberryPiRemote");
        }

        public string getPADMachineCodeFromRaspberryPI()
        {
            try
            {
                return _agk.getAGKTwoFileContent("MessageFromClient.txt", "RaspberryPiRemote")[0];
            }
            catch(Exception i)
            {
                return "";
            }
        }

        public void runRaspberryPi()
        {
            try
            {
                System.Diagnostics.Process.Start(Environment.CurrentDirectory + "\\raspberryPiRemote.exe");
            }
            catch (Exception ex)
            {
                //Log.handleException(ex);
                Console.WriteLine(ex);
            }
        }


        //code setup:
        //PIN_TYPE
        //VALUE -> sending a value to the motor or function
        //PIN -> the pin of where the 
        public string checkHumiditySensor(string pin, ref bool _loop) //the loop is a way for the outside code calling this function to end the loop
        {
            try
            {
                _loop = true;
                string code = pin + "_HUMIDITYTEMP";
                this.sendPADMachineCodeToRaspberryPI(code);
                string result = "";
                while(result == "" || result == null && _loop == true)
                {
                    result = getPADMachineCodeFromRaspberryPI();
                }

                return result;
            }
            catch(Exception i)
            {
                //Log.handleException(i);
                Console.WriteLine(i.Message);
                return null;
            }
        }

        public string checkTemperatureSensor(string pin, ref bool _loop) //the loop is a way for the outside code calling this function to end the loop
        {
            try
            {
                _loop = true;
                string code = pin + "_HUMIDITYTEMP";
                this.sendPADMachineCodeToRaspberryPI(code);
                string result = "";
                while (result == "" || result == null && _loop == true)
                {
                    result = getPADMachineCodeFromRaspberryPI();
                }

                return result;
            }
            catch (Exception i)
            {
                Console.WriteLine(i.Message);
                return null;
            }
        }

        public string checkPIRsensor(string pin, ref bool _loop) //the loop is a way for the outside code calling this function to end the loop
        {
            try
            {
                _loop = true;
                string code = pin + "_PIR";
                this.sendPADMachineCodeToRaspberryPI(code);
                string result = "";
                while (result == "" || result == null && _loop == true)
                {
                    result = getPADMachineCodeFromRaspberryPI();
                }

                return result;
            }
            catch (Exception i)
            {
                Console.WriteLine(i.Message);
                return null;
            }
        }

        public string checkGasSensorPercentage(string pin, ref bool _loop) //the loop is a way for the outside code calling this function to end the loop
        {
            try
            {
                _loop = true;
                string code = pin + "_GAS";
                this.sendPADMachineCodeToRaspberryPI(code);
                string result = "";
                while (result == "" || result == null && _loop == true)
                {
                    result = getPADMachineCodeFromRaspberryPI();
                }

                return result;
            }
            catch (Exception i)
            {
                Console.WriteLine(i.Message);
                return null;
            }
        }

        public string checkUltrasonicSensor(string pin, ref bool _loop) //the loop is a way for the outside code calling this function to end the loop
        {
            try
            {
                _loop = true;
                string code = pin + "_ULTRASONIC";
                this.sendPADMachineCodeToRaspberryPI(code);
                string result = "";
                while (result == "" || result == null && _loop == true)
                {
                    result = getPADMachineCodeFromRaspberryPI();
                }

                return result;
            }
            catch (Exception i)
            {
                Console.WriteLine(i.Message);
                return null;
            }
        }

        public void setToggleToPin(string pin, string _output)
        {
            string code = pin + "_TOGGLE_" + _output;
            this.sendPADMachineCodeToRaspberryPI(code);
        }




    }
}