﻿#region using region

using System;
using System.IO;
using System.Security.Principal;

#endregion

namespace RaspberryPiRemoteCSharp
{
    internal class AGKInfoGrabber
    {
        private readonly string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments,
            Environment.SpecialFolderOption.None);

        public AGKInfoGrabber()
        {
            Console.WriteLine("agk dir: " + dir);
        }

        /// <summary>
        ///     To Grab an app game kit 1.0v file info from the AGK directory (AGK is used for seperate programs with PAD)
        /// </summary>
        /// <param name="filename">filename</param>
        /// <param name="application">the AGK application name</param>
        /// <returns>filename info</returns>
        public string[] getAGKFileContent(string filename, string application)
        {
            if (File.Exists(dir + "\\AGK\\" + application + "\\media\\" + filename))
            {
                var stuff = File.ReadAllLines(dir + "\\AGK\\" + application + "\\media\\" + filename);
                File.Delete(dir + "\\AGK\\" + application + "\\media\\" + filename);
                return stuff;
            }
            return null;
        }

        /// <summary>
        ///     To Grab an app game kit 2.0v file info from the AGK directory (AGK is used for seperate programs with PAD)
        /// </summary>
        /// <param name="filename">filename</param>
        /// <param name="application">the AGK application name</param>
        /// <returns>the filename content</returns>
        public string[] getAGKTwoFileContent(string filename, string application)
        {
            if (File.Exists(getAGKDirTwo(application, filename)))
            {
                var stuff = File.ReadAllLines(getAGKDirTwo(application, filename));
                File.Delete(getAGKDirTwo(application, filename));
                return stuff;
            }
            return null;
        }

        /// <summary>
        ///     To send info to the main AGK program that runs along side of PAD
        /// </summary>
        /// <param name="cmd">command</param>
        /// <returns>To see if it worked or not</returns>
        public bool sendCodeHelperCommand(string cmd)
        {
            try
            {
                string[] args = {cmd};
                //System.IO.File.WriteAllLines("console_cmd.txt", args);
                Console.WriteLine(getAGKDirTwo("RobotClientAppStore", "console_cmd.txt"));
                File.WriteAllLines(getAGKDirTwo("RobotClientAppStore", "console_cmd.txt"), args);
                return true;
            }
            catch (Exception i)
            {
                Console.WriteLine(i.Message);
                return false;
            }
        }

        /// <summary>
        ///     create AGK 1.0 file with content, to communicate to the AGK program
        /// </summary>
        /// <param name="filename">the filename</param>
        /// <param name="info">the file content (message)</param>
        /// <param name="application">the AGK program name</param>
        public void CreateAGKFileContent(string filename, string info, string application)
        {
            File.WriteAllText(dir + "\\AGK\\" + application + "\\media\\" + filename, info);
        }

        /// <summary>
        ///     create AGK 2.0 file with content, to communicate to the AGK program
        /// </summary>
        /// <param name="filename">the filename</param>
        /// <param name="info">the file content (message)</param>
        /// <param name="application">the AGK program name</param>
        public void CreateAGKTwoFileContent(string filename, string info, string application)
        {
            File.WriteAllText(getAGKDirTwo(application, filename), info);
        }

        /// <summary>
        ///     Get the AGK 1.0 directory as a string of a program
        /// </summary>
        /// <param name="application">The Application name</param>
        /// <returns>return AGK directory of AGK program</returns>
        public string getAGKDir(string application)
        {
            return dir + "\\AGK\\" + application + "\\media\\";
        }

        /// <summary>
        ///     If there is more then one line in the AGK 1.0 file. get one of the lines
        /// </summary>
        /// <param name="filename">filename</param>
        /// <param name="application">application name</param>
        /// <param name="index">the line number</param>
        /// <returns>the message</returns>
        public string getSubItemOfAGKFile(string filename, string application, int index)
        {
            if (File.Exists(dir + "\\AGK\\" + application + "\\media\\" + filename))
            {
                return File.ReadAllLines(dir + "\\AGK\\" + application + "\\media\\" + filename)[index];
            }
            return null;
        }

        /// <summary>
        ///     get the AGK 2.0 directory based on a program
        /// </summary>
        /// <param name="appName">the program name</param>
        /// <param name="filename">the filename name</param>
        /// <returns>the directory of the file</returns>
        public string getAGKDirTwo(string appName, string filename)
        {
            var security = WindowsIdentity.GetCurrent().Name;

            var security_name = security.Split('\\');
            return "C:\\Users\\" + security_name[1] + "\\AppData\\Local\\AGKApps\\" + appName + "\\media\\" + filename;
        }


    }
}